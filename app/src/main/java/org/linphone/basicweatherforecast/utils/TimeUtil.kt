package org.linphone.basicweatherforecast.utils

import android.text.format.DateFormat
import java.util.*

class TimeUtil {

    companion object {
        fun getTime(timestamp: Double?): String {
            val calendar = Calendar.getInstance(Locale.ENGLISH)
            calendar.timeInMillis = (timestamp!! * 1000L).toLong()
            return DateFormat.format("HH:mm", calendar).toString()
        }

        fun fullDate(timestamp: Double?): String {
            val calendar = Calendar.getInstance(Locale.ENGLISH)
            calendar.timeInMillis = (timestamp!! * 1000L).toLong()
            return DateFormat.format("E, dd MM yyyy", calendar).toString()
        }
    }
}