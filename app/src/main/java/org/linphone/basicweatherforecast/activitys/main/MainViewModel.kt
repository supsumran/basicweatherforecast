package org.linphone.basicweatherforecast.activitys.main

import android.util.Log
import android.widget.CompoundButton
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.google.gson.Gson
import org.linphone.basicweatherforecast.activitys.forecast.ForecastActivity
import org.linphone.basicweatherforecast.MyApp
import org.linphone.basicweatherforecast.api.DataCallController
import org.linphone.basicweatherforecast.api.WeatherInterface
import org.linphone.basicweatherforecast.models.DataWeather

class MainViewModel : ViewModel() {

    companion object {
        private const val IMPERIAL_TYPE = "imperial"
        private const val METRIC_TYPE = "metric"
    }

    val isChecked = MutableLiveData<Boolean>()
    val type = MutableLiveData<String>()
    val isEmptyInputCity = MutableLiveData<Boolean>()
    val inputCity = MutableLiveData<String>()
    val cityTitle = MutableLiveData<String>()
    val isForecast = MutableLiveData<Boolean>()
    val temperature = MutableLiveData<String>()
    val humidity = MutableLiveData<String>()
    val teamLogoUri = MutableLiveData<String>()
    val lat = MutableLiveData<String>()
    val lon = MutableLiveData<String>()


    init {
        isChecked.value = false
        isEmptyInputCity.value = false
        inputCity.value = "london"
        isEmptyInputCity.value = inputCity.value.toString().isNotEmpty()
        setType(isChecked.value!!)
        onGetWeather()
    }

    fun executeOnStatusChanged(switch: CompoundButton, isChecked: Boolean) {
        setType(isChecked)
        onGetWeather()
    }

    fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) {
        inputCity.value = s.toString()
        isEmptyInputCity.value = inputCity.value.toString().isNotEmpty()
    }

    private fun setType(isChecked: Boolean) {
        if (isChecked) {
            type.value = "°F"
        } else {
            type.value = "°C"
        }
    }

    fun onGetWeather() { isEmptyInputCity.value = false
        DataCallController.getDataWeather(getTypeUnits(), inputCity.value.toString(),
            object : WeatherInterface.OnDataWeatherListener {
                override fun onSuccess(data: DataWeather) {
                    temperature.value = data.main!!.temp.toString()
                    humidity.value = data.main!!.humidity.toString()
                    cityTitle.value = data.name
                    isForecast.value = true
                    isEmptyInputCity.value = true
                    teamLogoUri.value =
                        "http://openweathermap.org/img/wn/" + data.weather!![0].icon + "@2x.png"
                    lat.value = data.coord!!.lat.toString()
                    lon.value = data.coord!!.lon.toString()
                }

                override fun onFail(err: String) {
                    cityTitle.value = err
                    isForecast.value = false
                    isEmptyInputCity.value = true
                    temperature.value= "0"
                    humidity.value = "0"
                }
            })
    }

    private fun getTypeUnits(): String {
        return if (type.value.equals("°F")) {
            IMPERIAL_TYPE
        } else {
            METRIC_TYPE
        }
    }

    fun onGoTo() {
        ForecastActivity.newIntent(MyApp.getContext()!!, lat.value!!, lon.value!!, getTypeUnits(), cityTitle.value!!)
    }
}