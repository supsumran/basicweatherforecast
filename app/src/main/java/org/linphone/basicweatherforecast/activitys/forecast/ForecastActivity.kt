package org.linphone.basicweatherforecast.activitys.forecast

import android.content.Context
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import com.bumptech.glide.Glide
import kotlinx.android.synthetic.main.forecast_activity.*
import org.linphone.basicweatherforecast.R
import org.linphone.basicweatherforecast.activitys.forecast.adapter.ForecastAdapter
import org.linphone.basicweatherforecast.databinding.ForecastActivityBinding


class ForecastActivity : AppCompatActivity() {

    lateinit var binding: ForecastActivityBinding
    lateinit var viewModel: ForecastViewModel
    lateinit var forecastAdapter : ForecastAdapter

    companion object {
        fun newIntent(context: Context, lat: String, lon: String, units: String, city: String) {
            var intent = Intent(context, ForecastActivity::class.java)
            intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK
            intent.putExtra("lat", lat)
            intent.putExtra("lon", lon)
            intent.putExtra("units", units)
            intent.putExtra("city", city)
            context.startActivity(intent)
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.forecast_activity)
        binding.lifecycleOwner = this
        viewModel = ViewModelProvider(
            this,
            ForecastViewModelFactory(
                intent.getStringExtra("lat")!!,
                intent.getStringExtra("lon")!!,
                intent.getStringExtra("units")!!,
                intent.getStringExtra("city")!!
            )
        )[ForecastViewModel::class.java]
        binding.forecastViewmodel = viewModel
        binding.back.setOnClickListener {
            finish()
        }

        viewModel.teamLogoUri.observe(this) {
            Glide.with(this).load(Uri.parse(it)).into(icon)
        }

        initForecastView()
    }

    private fun initForecastView() {
        forecastAdapter = ForecastAdapter(viewModel.type.value!!)
        forecast_list.apply {
            forecast_list.layoutManager =
                LinearLayoutManager(this@ForecastActivity, LinearLayoutManager.HORIZONTAL, false)
            adapter = forecastAdapter
        }
        viewModel.forecastHourlyList.observe(this){
            forecastAdapter.setData(it)
        }
    }
}