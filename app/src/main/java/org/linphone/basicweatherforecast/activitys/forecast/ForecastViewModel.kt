package org.linphone.basicweatherforecast.activitys.forecast

import android.util.Log
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import org.linphone.basicweatherforecast.api.DataCallController
import org.linphone.basicweatherforecast.api.WeatherInterface
import org.linphone.basicweatherforecast.models.DataForecastWeather
import org.linphone.basicweatherforecast.models.Hourly
import org.linphone.basicweatherforecast.utils.TimeUtil

class ForecastViewModelFactory(
    private val lat: String,
    private val lon: String,
    private val units: String,
    private val city: String
) : ViewModelProvider.NewInstanceFactory() {
    override fun <T : ViewModel> create(modelClass: Class<T>): T {
        return ForecastViewModel(lat, lon, units, city) as T
    }
}

class ForecastViewModel(
    private val lat: String,
    private val lon: String,
    private val units: String,
    city: String
) : ViewModel() {

    val teamLogoUri = MutableLiveData<String>()
    val cityTitle = MutableLiveData<String>()
    val temperature = MutableLiveData<String>()
    val humidity = MutableLiveData<String>()
    val type = MutableLiveData<String>()
    var forecastHourlyList = MutableLiveData<List<Hourly>>()
    val fullDateTime = MutableLiveData<String>()

    init {
        cityTitle.value = city
        if (units == "imperial"){
            type.value = "°F"
        } else {
            type.value = "°C"
        }
        getForecast()
    }

    private fun getForecast() {
        DataCallController.getForecastWeather(lat, lon, units, object :
            WeatherInterface.OnDataForecastWeatherListener {
            override fun onSuccess(data: DataForecastWeather) {
                teamLogoUri.value = "http://openweathermap.org/img/wn/" + data.current!!.weather!![0].icon + "@2x.png"
                temperature.value = data.current!!.temp.toString()!!
                humidity.value = data.current!!.humidity.toString()
                forecastHourlyList.value = data.hourly
                fullDateTime.value = TimeUtil.fullDate(data.current!!.dt)
            }

            override fun onFail(err: String) {
                Log.d("ForecastViewModel", "units : $err")
            }

        })
    }
}