package org.linphone.basicweatherforecast.activitys.forecast.adapter

import android.net.Uri
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import kotlinx.android.synthetic.main.forecast_item.view.*
import org.linphone.basicweatherforecast.MyApp
import org.linphone.basicweatherforecast.R
import org.linphone.basicweatherforecast.models.Hourly
import org.linphone.basicweatherforecast.utils.TimeUtil

class ForecastAdapter(private val type: String) :
    RecyclerView.Adapter<ForecastAdapter.ViewHolder>() {

    var hourly: List<Hourly>
    init {
        hourly = arrayListOf()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(
            LayoutInflater.from(parent.context)
                .inflate(R.layout.forecast_item, parent, false)
        )
    }

    fun setData(hourly: List<Hourly>) {
        this.hourly = hourly
        notifyDataSetChanged()
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(hourly[position], type)
    }

    override fun getItemCount(): Int {
        return hourly.size
    }

    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        fun bind(hourly: Hourly, type: String) {
            itemView.time.text = TimeUtil.getTime(hourly.dt)
            itemView.weather.text = hourly.temp.toString()
            itemView.type.text = type
            itemView.humidity.text = hourly.humidity.toString()

            val imageUrl = "http://openweathermap.org/img/wn/" + hourly.weather!![0].icon + "@2x.png"
            Glide.with(MyApp.getContext()!!).load(Uri.parse(imageUrl)).into(itemView.icon)
        }

    }
}