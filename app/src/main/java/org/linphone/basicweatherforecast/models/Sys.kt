package org.linphone.basicweatherforecast.models

import com.google.gson.annotations.Expose

import com.google.gson.annotations.SerializedName


class Sys {
    @SerializedName("type")
    @Expose
    var type: Double? = null

    @SerializedName("id")
    @Expose
    var id: Double? = null

    @SerializedName("country")
    @Expose
    var country: String? = null

    @SerializedName("sunrise")
    @Expose
    var sunrise: Double? = null

    @SerializedName("sunset")
    @Expose
    var sunset: Double? = null
}