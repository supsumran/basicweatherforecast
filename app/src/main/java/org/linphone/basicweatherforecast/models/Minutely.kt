package org.linphone.basicweatherforecast.models

import com.google.gson.annotations.Expose

import com.google.gson.annotations.SerializedName


class Minutely {
    @SerializedName("dt")
    @Expose
    var dt: Double? = null

    @SerializedName("precipitation")
    @Expose
    var precipitation: Double? = null
}