package org.linphone.basicweatherforecast.models

import com.google.gson.annotations.Expose

import com.google.gson.annotations.SerializedName


class Rain {
    @SerializedName("1h")
    @Expose
    private var one_h: Double? = null
}