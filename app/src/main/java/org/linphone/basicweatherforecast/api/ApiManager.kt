package org.linphone.basicweatherforecast.api

import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

class ApiManager {
    private val baseUrl = "https://api.openweathermap.org/data/2.5/"
    var service: ApiService? = null
        get() {
            if (field == null) {
                val retrofit: Retrofit = Retrofit.Builder()
                    .baseUrl(baseUrl)
                    .addConverterFactory(GsonConverterFactory.create())
                    .build()
                field = retrofit.create(ApiService::class.java)
            }
            return field
        }
}