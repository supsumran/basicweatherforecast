package org.linphone.basicweatherforecast.api

import org.linphone.basicweatherforecast.models.DataForecastWeather
import org.linphone.basicweatherforecast.models.DataWeather
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class DataCallController {
    companion object {
        private const val appID = "acd01c2056ce6243f40f548b65081b8b"
        private val manager = ApiManager()
        fun getDataWeather(
            units: String,
            city: String,
            listenerOn: WeatherInterface.OnDataWeatherListener
        ) {
            val call: Call<DataWeather> = manager.service!!.getWeather(units, city, appID)
            call.enqueue(object : Callback<DataWeather> {
                override fun onResponse(call: Call<DataWeather>, response: Response<DataWeather>) {
                    if (response.isSuccessful) {
                        val weather: DataWeather = response.body()!!
                        listenerOn.onSuccess(weather)
                    } else {
                        listenerOn.onFail(response.message())
                    }
                }

                override fun onFailure(call: Call<DataWeather>, t: Throwable) {
                    listenerOn.onFail(t.message.toString())
                }
            })
        }

        fun getForecastWeather(
            lat: String,
            lon: String,
            units: String,
            listener: WeatherInterface.OnDataForecastWeatherListener
        ) {
            val call: Call<DataForecastWeather> =
                manager.service!!.getForecastWeather(lat, lon, units, appID)

            call.enqueue(object : Callback<DataForecastWeather> {
                override fun onResponse(
                    call: Call<DataForecastWeather>,
                    response: Response<DataForecastWeather>
                ) {
                    if (response.isSuccessful) {
                        val data : DataForecastWeather = response.body()!!
                        listener.onSuccess(data)
                    } else {
                        listener.onFail(response.message())
                    }
                }

                override fun onFailure(call: Call<DataForecastWeather>, t: Throwable) {
                   listener.onFail(t.message.toString())
                }
            })
        }
    }
}