package org.linphone.basicweatherforecast.api

import org.linphone.basicweatherforecast.models.DataForecastWeather
import org.linphone.basicweatherforecast.models.DataWeather

class WeatherInterface {
    interface OnDataWeatherListener {
        fun onSuccess(data: DataWeather)
        fun onFail(err: String)
    }

    interface OnDataForecastWeatherListener {
        fun onSuccess(data : DataForecastWeather)
        fun onFail(err: String)
    }
}