package org.linphone.basicweatherforecast.api

import org.linphone.basicweatherforecast.models.DataForecastWeather
import org.linphone.basicweatherforecast.models.DataWeather
import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Query

interface ApiService {

    @GET("weather")
    fun getWeather(@Query("units") units : String, @Query("q") city:String, @Query("APPID") appID:String): Call<DataWeather>

    @GET("onecall")
    fun getForecastWeather(@Query("lat") lat:String, @Query("lon") lon:String, @Query("units") units:String, @Query("APPID") appID:String): Call<DataForecastWeather>
}
